# M129 - LAN-Komponenten in Betrieb nehmen 

Eine Übersicht über die im Modul 129 zu erreichenden Kompetenzen finden Sie im Abschnitt [Kompetenzmatrix](01_Kompetenzmatrix/)

# Unterlagen
  - Gitrepository [ch-tbz-it/Stud/m129](https://gitlab.com/ch-tbz-it/Stud/m129)
 - Unterlagen die aus urheberrechtlichen Gründen nicht hier veröffentlicht werden können, sind im MS Teams oder im [Sharepoint](https://tbzedu.sharepoint.com/:f:/r/sites/campus/students/it/_read-only?csf=1&web=1&e=6bj3Gs) zu finden. 
 - Alternative Unterlagen vom Modulverantwortlichen ARJ ohne GNS3 dafür mit Packettracer findet man im [Sharepoint](https://tbzedu.sharepoint.com/:f:/r/sites/campus/students/it/_read-only/M129-ARJ?csf=1&web=1&e=FE43aP)

# Weiterführende Links
 - Dokumentation in Markdown mit GitLab: https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/02_git
 - BeASwitch / BeARouter: https://github.com/muqiuq/BeASwitch
 - Unterlagen zu Netzwerkthemen von Jürg Arnold: https://www.juergarnold.ch
 - IPv4 Calculator http://jodies.de/ipcalc
 - Visual Subnet Calculator https://www.davidc.net/sites/default/subnets/subnets.html

# Lizenz
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
