# M129 - GNS3 Lab - Installation <!-- omit in toc -->

Die aktuelle Installationsanleitung für die Verwendung von GNS3 mit der TBZ Cloud ist unter dem nachfolgenden Link zu finden:

[gitlab.com/ch-tbz-it/Stud/allgemein/tbzcloud-gns3/-/tree/main/02_Bedienungsanleitung](https://gitlab.com/ch-tbz-it/Stud/allgemein/tbzcloud-gns3/-/tree/main/02_Bedienungsanleitung/01_GNS3%20Installation)

