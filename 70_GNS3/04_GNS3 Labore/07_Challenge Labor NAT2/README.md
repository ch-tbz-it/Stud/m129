# 7. Labor 7: Labor mit zwei Router (NAT ins Internet), Debian Client
![Screenshot Labor6](media/Labor7.PNG)

# 8. Lernziele
 - Der/die Lernende kann einen DHCP-Client konfigurieren. 

## 8.1. Anforderungen
 - Alle Subnetze sind klar vermerkt und Abgrenzungen grafisch erkennbar
 - Auf PC1 ist lxde installiert
 - Surfen im Internet (mit Webbrowser) ist möglich.
 - Alle ausgehenden IP Pakete auf R1-ether2 werden "masqueraded" (NAPT)
 - Trial Lizenzen auf R1 und R2 sind aktiviert (MikroTik Account benötigt)
 - Netz A: Beliebiges IPv4 Subnet
 - Netz B: 192.168.X.0/24
   - X = 32 + Position in Klassenliste

### 8.1.1. Variation (benötigt keinen MikroTik Account):
 - Als R1 OPNsense oder pfSense einsetzen
 - Als R2 cisco Router einsetzen

* Windows PC ist optional
