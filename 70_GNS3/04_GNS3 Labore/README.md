# GNS3 Labore  <!-- omit in toc -->

Die nachfolgenden Laboraufgaben bauen aufeinander auf. Es empfiehlt sich die Labore nacheinander zu bearbeiten. 

 - [01 - Ping mit Switch](./01_Ping%20mit%20Switch/)
 - [02 - Ping mit Router](./02_Ping%20mit%20Router/)
 - [03 - Ping mit mehreren Routern](./03_Ping%20mit%20mehreren%20Routern/)
 - [04 - Route Aggregation](./04_Route%20Aggregation/)
 - [05 - Challenge Labor Routing](./05_Challenge%20Labor%20Routing/)
 - [06 - Challenge Labor NAT](./06_Challenge%20Labor%20NAT/)
 - [07 - Challenge Labor NAT2](./07_Challenge%20Labor%20NAT2/)