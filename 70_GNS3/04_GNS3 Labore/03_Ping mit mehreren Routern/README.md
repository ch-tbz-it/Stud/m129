# Labor 3: Ping über Router (3 Subnetze) und lokalem PC
![Screenshot Labor1](media/Labor3.PNG)

## Lernziele
 - Routing von IPv4-Pakete über mehrere Router hinweg.
   - Konfiguration von statischen IPv4-Routen in einem MikroTik Router
   - Konfiguration von statischen IPv4-Routen unter Windows

## Anforderungen
- IPv4 Subnetz A ist wie folgt festgelegt:
   - 192.168.X.0/24
   - X = 10 + Position in Klassenliste
   - Bsp. X = 10 + 1 => 11 => 192.168.11.0/24
 - IPv4 Subnetz B wird wie Netz A festgelegt
   - X = 11 + Position in Klassenliste
   - Bsp. X = 20 + 1 => 21 => 192.168.21.0/24
 - PC2 kann PC1 anpingen und umgekehrt
 - PC1 und PC2 sind vom eigenen Laptop/PC anpingbar.
 - Netz C hat eine Maske /30
 - Alle Subnetze sind klar vermerkt und Abgrenzungen grafisch erkennbar

## Benötigte MikroTik Befehle

Neben den bisher gelernten Befehlen, werden für diese Aufgabe die folgende Befehle zsätzlich benötigt:

```
 /ip/route/print
```
Zeigt alle Einträge der Routing-Tabele an.

```
 /ip/route/add dst-address=192.168.1.0/24 gateway=10.10.10.1
```
- dst-address: Ziel-Netzwerk inkl. Subnetzmaske
- gateway: IP-Adresse des Next-Hop Routers

## Links
https://help.mikrotik.com/docs/display/ROS/IP+Routing