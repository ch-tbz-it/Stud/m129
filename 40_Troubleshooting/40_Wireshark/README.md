# Wireshark <!-- omit in toc -->

Die freie Netzwerk-Analyse-Tool [Wireshark](https://www.wireshark.org/) erlaubt es auf verschiedenen Kommunikationsmedien (Ethernet, Bluetooth, USB, usw.) Datenprotokolle zu analysieren. Mit diesem Tool können die Inhalte der übertragenen Datenströme in ihren einzelnen Bestandteilen analysiert werden. 

# 1. Praktische Übung - ARP Broadcast
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie können mithilfe von Wireshark Informationen wie MAC- und IP-Adresse auslesen. |

## 1.1. Einleitung

Bevor zwei Geräte über einen *Ethernet-Switch* mit IPv4 miteinander kommunizieren können, benötigen aus dem darunterliegenden Layer2 (Ethernet) die MAC Adresse des anderen Hosts. 

![GNS3 Labor Setup](media/GNS3LabSetup.PNG)

Möchte der PC1 den PC2 mit der IPv4-Adresse anpingen (ICMP Echo Request) benötigt er dessen MAC-Adresse.

![GNS3 Ping](media/PC1PC2ping.PNG)

Wenn wir uns den *ping command* genau anschauen, stellen wir fest, dass wir die MAC-Adresse nicht angeben. Wie weiss der PC1 nun, welche MAC Adresse er verwenden soll?

Da kommt das ARP-Protokoll ins Spiel.

![ARP Wireshark](media/Wireshark_ARP_ICMP_Ping.PNG)

In diesem YouTube Video von CertBros wird das ARP Protokoll erklärt: https://www.youtube.com/watch?v=tXzKjtMHgWI

## 1.2. Aufgabenstellung
 1. Bauen Sie sich sich das Labor wie oben dargestellt in GNS3 nach. Verwenden Sie als Switch einen *MikroTik Router* und passen Sie das Symbol entsprechend an. 
 2. Passen Sie die IPv4-Adresse der drei PCs an.<br/>
![GNS3 IPv4 Adresse anpassen](media/Vpcsipv4anpassen.webm)
 1. Konfigurieren Sie den MikroTik Router. Die notwendigen Befehle, um eine virtuelle Bridge zwischen den Ports 2,3 und 4 zu erstellen finden Sie weiter unten. 
 2. Starten Sie Wireshark auf der Verbindung zwischen dem PC1 und ether2 des MikroTik Routers. 
 3. Sobald Wireshark gestartet ist, pingen Sie den PC2 an. <br/>
![GNS3 Wireshark und ping](media/WiresharkpingarpVpcs.webm)
 6. Beantworten Sie mithilfe des aufgezeichneten *Trace* anschliessend die Fragen. Die Antworten notieren Sie am besten in ihrem persönlichen Portfolio. 
 7. Besprechen Sie die Antworten mit ihren Kollegen und der Lehrperson.

## 1.3. Fragen
Alle nachfolgenden Fragen können mithilfe der zuvor gemachten Aufzeichnung (Trace) mit Wireshark beantwortet werden:
 1. Wie viele ARP Pakete werden insgesamt übertragen?
 2. Wie lautet die MAC-Adresse von PC1?
 3. Wie lautet die MAC-Adresse von PC2?
 4. Wie viele Nutzdaten (Bytes) werde in einem *ICMP Echo-Request* übertragen?
 5. Was für (Menschen) lesbare Zeichen sind in den Nutzdaten in einem *ICMP Echo-Request* erhalten?
 6. Wie gross ist ein *ICMP Echo-Request* Frame insgesamt?
 7. Sind die verwendeten MAC-Adressen *Globally Unique*?
 8. Wie lautet der Name und der HEX-Wert des in Ethernet *encapsulated protocol* im ICMP Paket?
 9. Welcher "OSI-Layer" kommt in den *ARP-Frames* im Vergleich zu den *ICMP-Paketen* nicht vor?
 10. Welcher OP-Code hat der *ARP-Reply*?

## 1.4. Challange
 - Versuchen Sie einen ARP-Broadcast mit entsprechender Antwort in einem physischen oder produktiven Netzwerk abzufangen. 

# 2. Praktische Übung - MAC Tabelle
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | Sie können auf einem Switch oder Router die aktuelle (dynamische) MAC-Tabelle auslesen. |

**Hinweis:** Diese Übung baut der der vorherigen Übung auf. 

## 2.1. Aufgabe 1 - GNS3 MikroTik
![MikroTik Show MAC Table](media/MikroTikMacTable.PNG)
1. Loggen Sie sich auf dem MikroTik Router ein.
2. Lassen Sie sich die MAC-Tabelle anzeigen. 

## 2.2. Aufgabe 2 - Managed Switch
Im Schulzimmer stehen Ihnen verschiedene Switche zur Verfügung. Loggen Sie sich über das Webinterface oder via Telnet auf dem Switch ein und lesen die MAC-Tabelle aus. Recherchieren Sie benötigte Anleitungen selbstständig im Internet. 

