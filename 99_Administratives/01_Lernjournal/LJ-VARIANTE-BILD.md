# Lernjournal - Variante BILD

Das Lernjournal besteht aus zwei Komponenten:
 - Elektronisches Lernjournal (z.B. GitHub/GitLab Repository, geteiltes Notion.so, Geteiltes Dokument, usw.). Teilen Sie die Ressource mit der Lehrperson und tragen Sie den Link zu der Ressource in der Linkliste ein. 
 - Bild & Satz des Tages

### Elektronisches Lernjournal

Im Lernjournal ist festzuhalten:
 - Link zu bearbeiteten Lerneinheiten (mit Datum)
 - Beantwortete Fragen mit den dazugehörigen Antworten

Das Lernjournal muss stets aktuell gehalten werden. 

### Bild des Tages

Zeichnen Sie ein Bild / Grafik / Skizze mit der für Sie wichtigsten Erkenntnis des Tages. Beschreiben Sie diese Erkenntnis in einem Satz unter dem Bild. 

Die Vorlage erhalten Sie von der Lehrperson auf Papier. Ziel ist es bis zum Ende des Unterrichtsblockes das Bild erstellt und vorne abgegeben zu haben. 

Für diese Aufgabe sollten Sie nicht länger wie 15 Minuten aufwenden. 

