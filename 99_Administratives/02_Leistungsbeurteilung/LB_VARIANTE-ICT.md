# Leistungsbeurteilung

Welche Leistungsbeurteilung verwendet wird, hängt von der Lehrperson ab. 
In diesem Dokument ist die Leistungsbeurteilung `Variante ICT vom Repository` beschrieben. 


Die Schlussnote setzt sich wie folgt zusammen:
```
N = LB1*0.4 + LB2*0.4 + LB3*0.2
```

# LB1 - Schriftliche Prüfung
|   |   |
|---|---|
| Dauer  | 1 Lektion  |
| Erlaubte Hilfsmittel | Selbstgeschriebene Zusammenfassung (siehe Hinweise) |
| Zulassungsbedingung | Keine |
| Elektronische Hilfsmittel | Nicht erlaubt |

Der Prüfungsinhalt umfasst alle bis dahin besprochenen
 Unterlagen. Der definitive Prüfungsinhalt wird zwei Wochen vorher bekanntgegeben. 

Gewisse Aufgaben der schriftlichen Prüfung (IP-Berechnungsaufgaben) können wiederholt werden. Das bessere Resultat zählt. 

## Persönliche Zusammenfassung
 - Umfang:
   - Ausgedruckt: Max. 1 x A4 Papier (=4 Seiten)
   - Handgeschrieben: Max 2 x A4 Papier (=8 Seiten)
 - Individuelle und selbstgeschriebene Zusammenfassung
 - Gruppenarbeiten: Maximal 2 Personen
 - Keine „Collagen“: Aus den Unterlagen 1:1 übernommenen Text oder
Seitenausschnitte sind nicht zugelassen.  
 - Ausschnitte von einzelnen Grafiken / Schemas ist erlaubt.
 - Zusammenfassung muss mit der Prüfung abgegeben werden!

### **NICHT** erlaubte Inhalte
 - Umrechnungstabelle (mit Bytes und Masken)

# LB2 - Teilnahme an den Übungen

Die LB2 beurteilt ob die Übungen bearbeitet wurden, ob der Lernende sich ausreichend mit dem Thema beschäftigt hat. 

Pro bewertete Übung gibt es entweder 4 oder 2 Punkte. 

**Die Kriterien bei 4 Punkten sind die folgenden:**
| Nr.             | Kriterium                           | Punkte |
|-----------------|-------------------------------------|--------|
| **1**           | Lernender hat sich ausreichend mit Thema auseinandergesetzt und kann Fragen der Lehrperson beantworten.<br>Unzureichend (= 0P), Teilweise (=1P), Ausreichend (=2P) | 2 P |
| **2**            | Zeitnah abgegeben *                 | 1 P    |
| **3**            | Übung gelöst                        | 1 P    |

**Die Kriterien bei 2 Punkten sind die folgenden:**
| Nr.             | Kriterium                           | Punkte |
|-----------------|-------------------------------------|--------|
| **1**           | Zeitnah abgegeben *                 | 1 P    |
| **2**           | Übung gelöst                        | 1 P    |

*Zeitnah abgegeben:* Es wird erwartet, dass die Übungen fortlaufend bearbeitet und abgegeben werden. Beispiele:
 - Die Übung wird von Lehrperson in der ersten Woche empfohlen, und der/die Lernende gibt sie in der dritten Woche ab = Zeitnah abgegeben = 1 Punkt
 - Eine Einstiegsübung wird gegen Ende (in den letzten drei Wochen) abgegeben = Nicht zeitnah abgegeben = 0 Punkte

*Übung gelöst:* Die Übung gilt als gelöst, wenn diese selbst gelöst wurde und im Lernprodukt erkennbar ist, dass sich der Lernende mit dem Thema auseinandergesetzt hat. 

Zur Bewertung hinzugezogen werden: 
 - Lernjournal (Wird zur Beurteilung der Übungen hinzugezogen)
 - Bild des Tages (Pro abgegebenes Bild gibt es 1/2 Punkt, maximal 10 Punkte möglich)

# LB3 - Beurteilung der Lernbereitschaft durch Lehrperson

Überzeugen Sie mit Einsatz, Fleiss und Können die Lehrperson. Wenn es für die Lehrperson ersichtlich ist, dass sie sich «ins Zeug gelegt haben», alle grossen und kleinen Hürden mit Hartnäckigkeit und Geduld überwunden haben, dann überzeugen Sie damit die Lehrperson und erhalten entsprechend Punkte. Die Beurteilung erfolgt individuell und berücksichtigt folgendes:

-	Der/Die Lernende(r) hat sich mit den Themen intensiv auseinandergesetzt.
-	Der/Die Lernende(r) beschäftigt sich im Unterricht mit den zu behandelnden Themen.
-	Das Verhalten des/der Lernenden ist der Situation stets angemessen.
-	Der/Die Lernende(r) ist sich der Ziele bewusst und arbeitet auf diese zu.
-	Die vom Lernenden produzierten Produkte sind sauber und sorgfältig umgesetzt.
-	Der/Die Lernende(r) zeigt keine Anzeichen von Gleichgültigkeit. 
-	Der/Die Lernende(r) ist stets zum Unterrichtstart bereit und hört aufmerksam zu. 
-	Der/Die Lernende(r) lässt sich nicht und lenkt andere nicht vom Thema ab. 
-	Der/Die Lernende(r) behandelt seine Mitmenschen anständig und ist stets wohlwollend.

Die Beurteilung des persönlichen Einsatzes und der Lernbereitschaft erfolgt zweimal im Unterricht. Einmal nach ca. 4 Wochen und einmal am Ende des Moduls. Die Beurteilungen zählen jeweils zu 50%. 

Die Beurteilung wird per E-Mail an die Lernenden verschickt. 

Bemerke: Die Gesamtbeurteilung umfasst die gesamte Zeit. Das heisst: Die erste Zwischenbeurteilung hat einen Einfluss auf die Gesamtbeurteilung.
