# Leistungsbeurteilung Modul 129

Leistungsbeurteilungsvarianten:
 - [Repository Variante A](./LB_VARIANTEA.md)
 - [Repository Variante ICT](./LB_VARIANTE-ICT.md)

*Hinweis:* Informieren Sie sich bei(m) Kursleiter*in / Lehrperson, welche Leistungsbeurteilung für sie gültig ist. 
