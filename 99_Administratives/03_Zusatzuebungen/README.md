# Modulübergreifende Zusatzübungen

|Aufgabenbeschreibung   | Eingesetzt für Klassentyp   | Zeitpunkt | Link |
|---|---|---|---|
| Filius Hands on Aufgabe als Einstieg  | ICT | Am Afang (Block 1/2)  | [Link](https://gitlab.com/ch-tbz-it/Stud/m117/-/blob/main/Unterlagen/N1/m117_5_Filius-Hands-on.md?ref_type=heads)  |
| Erweiterte Filius Aufgabe  | ICT  | Am Anfang (für die schnellen)  | [Link](https://gitlab.com/ch-tbz-it/Stud/m117/-/tree/main/Unterlagen/N1/ressourcen/2-auftraege-LN/02)  |
|   |   |   |   |