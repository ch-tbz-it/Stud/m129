# Netzwerk Klassenchallenge  <!-- omit in toc -->

![Symbolbild](./media/symbolbild.webp)
<br>*Abbildung 0: Ein mit ChatGPT generiertes Symbolbild*

Diese Lerneinheit ist ein Challenge für die gesamte Klasse. Jeder Lernende konfiguriert einen Router und stellt Verbindungen zu Routern anderer Lernenden. Das Resultat ist ein grosses geroutet Netzwerk. 

Damit am Ende alle funktionalen Ziele erreicht werden und jeder Lernende von seinem Router aus beispielsweise Zugriff auf das Internet hat, sind nicht nur technische, sondern auch soziale Fähigkeiten gefragt. Die Klasse kann das Netzwerk nur durch Zusammenarbeit erfolgreich aufbauen. Jeder muss sich aktiv einbringen und kooperativ mit den anderen Lernenden zusammenarbeiten, um das gemeinsame Ziel zu erreichen. Diese kollaborative Herangehensweise fördert das gegenseitige Verständnis und die Fähigkeit, als Team effektiv zu arbeiten.

## 1. Lernziele dieser Lerneinheit
 - Vertiefung des erarbeiteten Wissens zu Fehlersuche, Analyse, Routing und Routerkonfiguration.
 - Effektive Zusammenarbeit und Teamarbeit beim Aufbau des Netzwerkes.


## 2. Lernziele gemäss Kompetenzmatrix
 - E1G: Ich kann grundlegende Befehle zur Fehlersuche anwenden.	
 - E1F: Ich kenne verschiedene Werkzeuge zur Analyse der Funktion des Netzwerkes und kann damit Fehlfunktionen eingrenzen und beheben.	
 - D1G: Ich kann einfache statische Routingtabellen umsetzen.
 - D1F: Ich kann die Vor- und Nachteile von statischen/dynamischen Routing erklären und statische Routingtabellen für ein LAN mit mehreren Subnetzen fachgerecht planen und umsetzen.
 - B1G: Ich kann die Funktionsweise von Switches und Routern erklären, sowie diese in Betrieb nehmen.
 - B1F: Ich kann wesentliche Eigenschaften eines Switches aufzeigen, die Funktionsweise eines Routers erklären und diese Geräte fachgerecht konfigurieren und in Betrieb nehmen. Zudem kann ich wichtige Sicherheitseinstellungen (Default Password, Grundlagen) vornehmen.

## 3. Funktionale Ziele
 - Alle Router können sich gegenseitig erreichen (RA1 kann RB2 erreichen (ping), RA3 kann RA4 erreichen usw.)
 - Jeder Router kann ins Internet zugreifen
 - Mit WLAN verbundene Clients können aufs Internet zugreifen


## 4. Netzwerkplan analysieren
![Netzwerktopologie](./media/Netzwerktopologie.png)
<br>*Abbildung 1: Topologie mit allen physischen Verbindungen und IP-Subnetzen*

Das Analysieren des Netzwerkplans ist wichtig, um eine klare Übersicht über die benötigten IP-Adressen und die Struktur des Netzwerks zu erhalten. Es hilft, die Anzahl und Grösse der Subnetze zu prüfen, um die Absicht des Netzwerkplaners nachvollziehen zu können. Aus dem Plan lässt sich weiter die benötigten Routen bestimmen. Insgesamt bildet der Netzwerkplan die wichtigste Grundlage für die Konfiguration des Netzwerkes. 

**💪 Challenge:** Analysiere die Topologie mithilfe folgender Fragen
 - Wie viele Client-Access Netzwerke gibt es?
 - Welche IP-Adresse hat `RA1` auf Port 4?
 - Welche IP-Adresse hat `RB2` auf Port 3?
 - Weshalb sind die Subnetze zwischen den Router `/30`?
 - Mit welchem übergeordneten Subnetz können alle Subnetze, welche abgehend von `R00` Port 5 sind, zusammengefasst werden?

## 5. Router-Zuteilung

**💪 Challenge:** Organisation und Planung im Team

Organisiere dich mit den anderen Lernenden in der Klasse, wer welchen Router konfigurieren wird. Ziel ist es, dass jeder Lernenden einen Router konfiguriert (Beispiel: Max konfiguriert RA1 und Lana konfiguriert RA2, usw.)

Ein oder mehrere Lernender bekommt die anspruchsvolle Aufgabe den zentralen Router **R00** konfigurieren. Dieser Router benötigt nebst der Konfiguration der IP-Adressen-Interface und IP-Routen auch noch weitere Konfigurationen (NAT, usw.). 

## 6. Hardware

**💪 Challenge:** Hardware besorgen

Besorge dir einen MikroTik Router des Typs *hAP ax lite*. Falls nicht verfügbar kannst du auch ein *hAP ac²* holen. 

Dieses universalen Netzwerkgerät kann wesentlich mehr als ein "Router". Grundsätzlich kann ein Router nur "Routen". In der Praxis verfügen die Geräte über viel mehr Funktionen. So können die in dieser eingesetzten Geräte auch Switching, VPN, PPPoE, Wireless und noch vieles mehr. 

Im Klassenchallenge werden wir folgende Funktionen einsetzen:
 - IP-Routing
 - DHCP Server
 - Wireless Access Point
 - DNS Forwarding

Besorgen Sie sich bei der Lehrperson:
 - Einen *hAP ax lite* oder *hAP ac²*
 - Benötigte Netzwerkkabel
 - USB-Ethernet Adapter, falls dein Laptop keine Ethernet Schnittstellen hat

## 7. Router vorbereiten

**💪 Challenge:** Router vorbereiten

Bereite deinen Router gemäss der Anleitung [Routing mit MikroTik - Einstieg](../02_Routing%20mit%20MikroTik/A_Einstieg/) vor. 

**Achtung**: Spiele die Standardkonfiguration (Schritt 5) **nicht** ein. Verwendende stattdessen die nachfolgende Anleitung für die Konfiguration des Routers. 

**Wichtig: Als Herausforderung wurden teilweise  bewusst Informationen oder Schritte weggelassen. Mithilfe des in den vorgehenden Übungen erarbeiteten Wissens sollten die Lernenden in der Lage sein, diese Herausforderungen zu meistern.**

## 8. Bridge anlegen

**💪 Challenge:** Bridge konfigurieren

Auf dem Router wird ein virtueller Switch (Bridge) angelegt für das *Client-Netzwerk*. Auf dieser Bridge wird die IP-Adresse des *Client-Netzwerkes* konfiguriert (siehe Netzwerkplan) und die physischen Interfaces *ether2* und *wifi1* angeschlossen. Verwende dafür das Terminal in WinBox um die nachfolgenden Befehle auszuführen (**Hinweis:** Die IP-Adresse des *Client-Netzwerkes* muss im letzten Befehl eingetragen werden und im ersten Befehl der Name des Routers ):

```MikroTik
/system/identity/set name=NAMEDESROUTERS
/interface bridge add name=bridge
/interface bridge port add bridge=bridge interface=ether2
/interface bridge port add bridge=bridge interface=wifi1
/ip address add address=CLIENT_SUBNETZ interface=bridge
```

**Alternativ** kann diese Konfiguration manuell über die WinBox GUI vorgenommen werden:

![Identity Bridge konfigurieren](./media/Klassenchallenge_Identity_Bridge.webm)

[Videoanleitung *Identity und Bridge konfigurieren* downloaden](./media/Klassenchallenge_Identity_Bridge.webm)

## 9. DHCP-Server im Client-Netzwerk konfigurieren

**💪 Challenge:** DHCP- und DNS-Server konfigurieren

Damit die Clients im *Client-Netzwerk* automatisch eine IP-Adresse erhalten, muss der DHCP-Service auf der virtuellen Bridge konfiguriert werden. Verwende das Video **oder** die Befehle, um den DHCP-Server auf der Bridge konfigurieren und DNS Anfragen zu erlauben. 

```MikroTik
/ip pool add name=dhcp_pool1 ranges=START_ADRESSE-END_ADRESSE
/ip dhcp-server add address-pool=dhcp_pool1 interface=bridge name=dhcp1
/ip dhcp-server network add address=CLIENT_SUBNETZ dns-server=CLIENT_HOSTADRESSE gateway=CLIENT_HOSTADRESSE
/ip dns set allow-remote-requests=yes
```

![Identity Bridge konfigurieren](./media/Klassenchallenge_DHCP_DNS.webm)

[Videoanleitung *DHCP- und DNS-Server konfigurieren* downloaden](./media/Klassenchallenge_DHCP_DNS.webm)

## 10. IP-Adressen auf Uplink- und "Downlink"-Ports konfigurieren

**💪 Challenge:** Zwei IP-Adressen gemäss Netzwerkplan konfigurieren

Alle Router benötigen einen Uplink-Port (Port der verwendet wird um aufs Internet zuzugreifen) und einen Port den anderen Router für ihren Uplink verwenden können. Die IP-Adresse für den eigenen Router kann der Netzwerktopologie entnommen werden. 

![Uplink Ports](./media/UplinkPorts.png)
<br>*Abbildung 2: Topologie mit allen markierten Uplink-Ports*

**Beispiel: Router BSP**

![Example Router BSP](./media/ExampleRouter.png)
<br>*Abbildung 3: Beispiel Topologie*

Auf dem Router sind zwei Adressen zu konfigurieren:
 - Auf Ether4 10.15.1.2/30
 - Auf Ether3 10.15.1.5/30

**Achtung: Das sind Beispiel-Adressen!**

![IP-Adresse zuweisen](./media/IPAdresseKonfigurieren.webm)

[Videoanleitung *IP-Adresse zuweisen* downloaden](./media/IPAdresseKonfigurieren.webm)


## 11. Mit anderen Router verbinden

**💪 Challenge:** Verbinde deinen Router gemäss Netzwerktopologie mit anderen Router. 

**Beispiel: RB2**
 - *Port 3 RB2* mit *Port 4 auf RB3* verbinden
 - *Port 4 RB2* mit *Port 3 auf RB1* verbinden

**💪 Challenge:** Ping direkt verbundene Router an. Kannst du die Router erfolgreich anpingen? Die IP-Adressen der benachbarten Router sind von der Netzwerktopologie zu entnehmen. 

**Beispiel: RB2**
 - RB1 anpingen auf `10.11.64.5`
 - RB3 anpingen auf `10.11.64.10`


**Bemerkung:** Erst nachdem die benachbarten Router angepingt werden können, werden die im nächsten Abschnitt konfigurierten Routen funktionieren. Es ist deshalb essenziell zuerst sicherzustellen, dass  benachbarte Router sich gegenseitig anpingen können. Koordiniere dich mit den anderen Lernenden. 

## 12. Routen konfigurieren

Für alle nicht direkt an einem Router anliegenden Subnetze müssen auf dem Router *Routen* angelegt werden. Schauen wir das am Beispiel vom Router RA4 an:
 - Der Router benötigt eine Default Route an einem Uplink-Port. Der nächste HOP ist RA3.<br>Destination-Address: 0.0.0.0/0 Gateway: 10.11.0.13
 - Der Router benötigt eine Route zu 10.11.0.20/30, 10.11.5.0/24 und 10.11.6.0/24 via RA5<br>Destination-Address: 10.11.0.20/30 Gateway: 10.11.0.18<br>Destination-Address: 10.11.5.0/24 Gateway: 10.11.0.18<br>Destination-Address: 10.11.6.0/24 Gateway: 10.11.0.18

Im nachfolgenden Video siehst du wie eine Route in WinBox konfiguriert werden kann:

![IP-Route anlegen](./media/RouteKonfigurieren.webm)

[Videoanleitung *IP-Route konfigurieren* downloaden](./media/RouteKonfigurieren.webm)

**💪 Challenge:** Lege mithilfe der Netzwerktopologie fest für welche Subnetze Routen angelegt werden müssen. Lege für jedes Subnetz den nächsten Hop (Gateway) fest. 

## 13. Testen - Ping und Traceroute

Teste mit *Ping* und *Traceroute* ob du Zugriff ins Internet hast. 

![Example Router BSP](./media/Traceroute.PNG)
<br>*Abbildung 4: Beispiel Traceroute*

- Weshalb ist *Traceroute* das "bessere Ping"?

## 14. WiFi konfigurieren

**💪 Challenge:** Konfiguriere das *WiFi1* Interface auf deinem Router. Es müssen folgende Parameter festgelegt werden:
 - SSID (Name des WiFi Netzwerkes)
 - Passphrase (Vorschlag: `tbz123456` )
 - Authentication Mechanism
 - Frequenz
 - Land
 - Band

Verwende als SSID den Namen deines Routers. 

![WiFi-Konfigurieren](./media/WiFiKonfigurieren.webm)

[Videoanleitung *WiFi-Konfigurieren* downloaden](./media/WiFiKonfigurieren.webm)

## 15. Testen - Client-Zugriff aufs Internet

**💪 Challenge:** Verbinde dich mit deinem Smartphone mit dem zuvor erstellen WiFi-Netzwerk. Hast du Zugriff ins Internet? Wenn nicht: Mache dich auf auf zur Fehlersuche! 

## 16. Abschluss

Die Klasse hat die Challenge erfolgreich abgeschlossen, wenn
 - aus jedem Clientnetzwerk (WiFi) der Zugriff ins Internet möglich ist.
 - jeder Router jeden anderen Router anpingen kann. 
 - jeder Router einen Host im Internet (z.B. `9.9.9.9` oder `tbz.ch`) anpingen kann. 