# Routing mit dem Cisco Packet Tracer  <!-- omit in toc -->

Diese Übung soll mit kleinen Herausforderungen das Thema IPV4-Routing näher bringen. Dabei wird das Konfigurieren von IPv4-Adresse auf Netzwerkinterfaces und das Konfigurieren von statischen Routen geübt. 

## 1. Ohne ARP kein IPv4 - Ohne IPv4 kein Ping

![Laboraufbau](media/lab1.png)
<br>*Abbildung 1: Laboraufbau*

Bevor ein Ping bzw. ein *ICMP Echo Request* über IPv4 verschickt werden kann, müssen die Anforderungen der darunterliegenden Layer erfüllt sein. Wenn Netzwerkgeräte über Netzwerkkabel verbunden sind, kommt dabei meist das Protokoll *Ethernet II* zu tragen, welches für die Adressierung MAC-Adressen verwendet. Oder kurz: Die MAC-Adresse des Empfängers muss bekannt sein. 

Wenn ich von meinem Notebook zu Hause ein Paket zu Google sende, muss mein Computer jedoch nur die MAC-Adresse des Routers kennen. 

![Ping two Subnets](media/PingIntoAnotherSubnet.webm)

**💪 Challenge 1:** Schaue dir das kurze Video an und beantworte anschliessend nachfolgende Fragen. Zusätzlich zum Video steht das Cisco Packet Tracer Labor zur Verfügung, welches im Labor verwendet wurde. 

1. Wie viele ARP-Request wurden durchgeführt?

2. Warum kennt PC0 die MAC-Adresse von PC1 nicht und weshalb benötigt PC0 diese MAC-Adresse nicht?

3. Weshalb schlägt der erste Ping fehl?

### 1.1. Ressourcen

 - 🎞️ Video: Cisco Packet Tracer: [Ping über Router](media/PingIntoAnotherSubnet.webm)
 - [🧪 Cisco Packet Tracer Labor](./Labor1.pkt)


## Aus zwei mach drei - 3 Hops

![Laboraufbau](media/lab2.png)
<br>*Abbildung 2: Laboraufbau*

**💪 Challenge 2:** Baue das Labor aus der ersten Übung so aus, dass zwischen PC0 und PC1 zwei Router sind und sich die beiden PCs erfolgreich gegenseitig pingen können. Schaue dazu zuerst das Video an, wie das Labor aus dem ersten Teil um einen Router erweitert wird. 

Ausgangslage für Challenge 2: [🧪 Cisco Packet Tracer Labor](./Labor2.pkt)

![Extend Lab 1 with another Router](media/ExtendWithAnotherRouter.webm)



