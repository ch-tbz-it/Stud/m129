# Routing

Dieser Abschnitt enthält praktische Übungen zum Thema Routing. 

Als Voraussetzung für diese Übungen werden die Inhalte aus dem vorhergehenden Kapitel [Subnetting und Routinggrundlagen](../20_Subnetting%20and%20Routing/) empfohlen. 

