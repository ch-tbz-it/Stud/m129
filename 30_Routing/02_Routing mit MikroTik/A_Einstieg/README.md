# Einstieg - Routing mit MikroTik  <!-- omit in toc -->

Dieser Abschnitt beinhaltet die Grundkonfiguration des Routers, sodass er anschliessend für die Aufgaben "Routen mit einem Router" und "Router mit mehreren Routern" verwendet werden kann. 

## 1. Voraussetzungen
 - MikroTik hAP ax lite (o.ä.)
 - Patch-Kabel
 - Windows Notebook mit RJ45 Buchse

## 2. MikroTik zurücksetzen

1. MikroTik Router von der Stromversorgung **trennen**.
2. Reset-Taste **gedrückt halten**
3. MikroTik Router mit der Stromversorgung **verbinden**
4. Sobald die `usr-LED` fünfmal geblinkt hat, `reset` loslassen.

## 3. Mit WinBox verbinden und Standardkonfiguration löschen

![winbox](media/winbox.png)

1. Auf der MikroTik Webseite Winbox [herunterladen](https://mikrotik.com/download). 
2. MikroTik Router (Port 2) mit dem eigenen Notebook verbinden.
3. Nach einer Zeit wird der MikroTik Router unter `Neighbors` angezeigt.
4. Auf `MAC Adresse` doppelt klicken
5. Standard Passwort steht bei neueren MikroTik Geräten auf dem Gehäuse. 
6. Mit `Connect` verbinden. 
7. `Remove Configuration` auswählen, um Standardkonfiguration zu löschen.
8. ca. 1 Minute warten, der Router wird sich trennen und versuchen neu zu verbinden. Das neue Standardpasswort ist leer. 
9. Neues Passwort auf `admin` setzen und mit `Change Now` speichern. 
10. Trenne die Verbindung unter *Session* => *Disconnect* und verbinde dich mit dem neuen Passwort. 

![Remove Default Configuration](media/removeconfiguration.png)

## 4. IPv4 Subnetz bestimmen
In der Aufgabe "Router mit mehreren Routern" verbinden die Lernenden ihre Router miteinander. Damit es zu keinem IPv4-Adresskonflikt kommt, benötigt es eine Subnetzverwaltung. Informiere dich bei der Lehrperson, wie die Subnetzverwaltung für die Klasse umgesetzt ist. Häufig findest du im Teams für die Klasse unter *Allgemein* => *Organisation* eine Excel Tabelle, welche ein Tab *Subnetze* oder *IPv4* hat.

## 5. Standardkonfiguration einspielen

**Unbedingt beachten: Das Script darf nur einmal ausgeführt werden!**

1. Die nachfolgende Konfiguration in einen Texteditor kopieren und alle X mit dem eigenen dritten Octet ersetzen.   
```routeros
/interface bridge add name=bridge
/interface bridge port add bridge=bridge interface=ether2
/ip address add address=192.168.X.1/24 interface=bridge network=192.168.X.0
/ip pool add name=dhcp_pool1 ranges=192.168.X.128-192.168.X.200
/ip dhcp-server add address-pool=dhcp_pool1 interface=bridge name=dhcp1
/ip dhcp-server network add address=192.168.X.0/24 dns-server=192.168.X.1 gateway=192.168.X.1
/ip dns set allow-remote-requests=yes
```
2. Im Winbox *System* => *Script* => `+` wählen und angepasster Script einfügen. 

![Script](media/newscript.png)

3. Script mit `Apply` abspeichern und ausführen mit `Run Script` ausführen. 
4. Anschliessend schliesst sich Winbox nach ein paar Sekunden und öffnet sich anschliessend erneut.
5. Das Script kann mit `Remove` gelöscht werden. 

![Remove Script](media/removescript.png)

## 6. Abschluss

Glückwunsch!

Der Router ist nun bereit für die Übung: Routen mit einem Router

