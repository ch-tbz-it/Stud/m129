# Routen mit einem Router <!-- omit in toc -->

![Labor Setup](media/laboverview.png)
*Abbildung 1: Topologie des Laborsetups*

## 1. Lernziele
 - B1G: Ich kann die Funktionsweise von Switches und Routern erklären, sowie diese in Betrieb nehmen.
 - D1G: Ich kann einfache statische Routingtabellen umsetzen.


## 2. Voraussetzungen
 - Ein vorbereiteter MikroTik hAP Lite (siehe Einstieg)
 - Zwei Notebooks mit Ethernetanschluss
 - Das eigene Notebook ist jeweils mit dem MikroTik Router verbunden und der Zugriff auf die Konfiguration mit WinBox ist möglich. 
 - WLAN auf beiden Notebooks deaktiviert

**Wichtig:** Dies ist eine Übung und **keine** Schritt-für-Schritt Anleitung. Gewisse Schritte und Informationen wurden für den Lerneffekt bewusst weggelassen. 

## 3. Windows Firewall Konfigurieren

In dieser Übung sollen die Notebooks sich gegenseitig anpingen. Standardmässig blockiert die Windows Firewall alle "Echo Request" bzw. Ping-Anfragen. 

**💪 Challenge 1:** Recherchiere selbstständig, wie du auf deinem Notebook eingehende Pings erlauben kannst oder verwende einer der verlinkten Anleitungen, um Ping-Anfragen in der Windows Firewall zu erlauben. 

### 3.1. 📕 Ressourcen

 - [Youtube - Ping in Windows 10 Firewall freischalten](https://www.youtube.com/watch?v=HUfZZwS_UWg)
 - [WindowsPro - Ping in Windows 10 erlauben: GUI, PowerShell, netsh, GPO](https://www.windowspro.de/wolfgang-sommergut/ping-windows-10-erlauben-gui-powershell-netsh-gpo)

## 4. Zweites Subnetz auf dem eigenen Router konfigurieren

1. In dieser Übung werden zwei Subnetze benötigt. Dein bestehendes Subnetz wird fort an Subnet X bezeichnet. Das zweite Subnetz Subnetz Y. ✏️ Reserviere in der Klassenliste ein weiteres Subnetz und verwende es als Subnetz Y.
2. IPv4-Adresse aus dem Interface `ether3` konfigurieren
![Neue Adresse](media/newaddress.png)

3. Verbinde das Notebook des Kollegen mit dem Port 3. Konfiguriere eine statische IPv4-Adresse aus dem Subnetz Y auf dem Notebook (Tipp: Windows+R => `ncpa.cpl`).

1. Versuche nun von deinem Laptop aus den anderen Laptop anzupingen. 

![ping](media/ping.png)

4. Versuche nun vom anderen Laptop aus den eigenen Laptop anzupingen. 

5. Versuche vom Router aus das Notebook anzupingen. 

![ping](media/pingmikrotik.png)

## 5. Abschluss

Wenn alle pings erfolgreich waren, hast erfolgreich über den eigenen Router hinweg "geroutet".