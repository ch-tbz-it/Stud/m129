# Routen mit mehreren Routern <!-- omit in toc -->

![Laboraufbau](media/lapsetup.png)
<br>*Abbildung 1: Topologie des Laborsetups*

In dieser Übung konzentrieren wir uns auf das IPv4-Routing über mehrere Netzwerkknoten hinweg. Die Teilnehmer lernen, wie Datenpakete durch eine Serie von Routern weitergeleitet werden und wie IPv4-Adressen und Routen konfiguriert werden. 

## 1. Lernziele
 - B1G: Ich kann die Funktionsweise von Switches und Routern erklären, sowie diese in Betrieb nehmen.
 - D1G: Ich kann einfache statische Routingtabellen umsetzen.

## 2. Vorbereitung
 - Zwei Lernende mit je einem MikroTik Router
 - Pro Lernender ein Notebook mit RJ45-Anschluss
 - Das eigene Notebook ist jeweils mit dem MikroTik Router verbunden und der Zugriff auf die Konfiguration mit WinBox ist möglich. 
 - WLAN auf beiden Notebooks deaktiviert

## 3. Netzwerk planen

Bevor mit der Konfiguration begonnen wird, soll die Netzwerkkonfiguration geplant werden. Dafür soll jeder Netzwerkschnittstelle eine IPv4-Adresse zugeteilt werden. Denke daran, dass ein Router pro Subnetz eine IPv4-Adresse benötigt. Reserviere alle verwendeten Subnetze in der Organisationsliste der Klasse (falls vorhanden). 

**💪 Challenge 1:** Erstelle mit [Draw.io](https://draw.io) oder [Microsoft Visio](https://www.microsoft.com/de-ch/microsoft-365/visio/flowchart-software) einen Netzwerkplan auf dem alle IPv4-Adressen eingetragen sind. Als Ausgangslage ist die Netzwerktopologie des Laborsetups zu verwenden (siehe Abbildung 1). 

## 4. IPv4-Adressen konfigurieren
**💪 Challenge 2:** Konfiguriere auf den MikroTik auf den entsprechenden Interfaces (bzw. Ports) die geplanten IPv4-Adressen. Pro MikroTik Router sollten jeweils 2 IPv4-Adressen konfiguriert sein. 

## 5. Router pingen sich gegenseitig

**💪 Challenge 3:** Pinge von Router `R1` zu Router `R2` und umgekehrt. Wenn sich die Router gegenseitig anpingen können, wurde erfolgreich ein Subnetz zwischen den beiden Routern in Betrieb genommen!

![Ping OK](media/pingok.png)
<br>*Abbildung 2: Erfolgreiche Pings*

## Notebook pingen sich gegenseitig

**💪 Challenge 4:** Versuche einen ping von Notebook `N1` (z.B. eigenes Notebook) zum Notebook `N2` (Notebook des Kollegen) durchzuführen. 


Dieser Ping sollte fehlschlagen. Weshalb schlägt der Ping fehl, obwohl auf allen Interfaces IPv4-Adressen konfiguriert wurden? Die Antwort darauf findet sich in der Routing-Tabelle.

![Routing-Tabelle R1](media/routingtabler1.png)
<br>*Abbildung 3: Beispiel Routing-Tabelle Router R1*

![Routing-Tabelle R2](media/routingtabler2.png)
<br>*Abbildung 4: Beispiel Routing-Tabelle Router R2*

Angenommen Notebook `N1` ist an Router `R1` angeschlossen und hat die IPv4-Adresse `192.168.10.10` und Notebook `N2` ist an Router `R2` angeschlossen und hat die IPv4-Adresse `192.168.11.10`. Wenn nun ein Ping von `N1` nach `N2` geschickt wird, lautet der Absender `192.168.10.10` und der Empfänger `192.168.11.10`. Sobald das Packet den Router `R1` erreicht, prüft dieser in seiner Routing-Tabelle (Siehe Abbildung 3) wohin er das Packet mit der Destination `192.168.11.10` schicken soll. Jedoch findet er in seiner Tabelle keinen passenden Eintrag. Das Packet wird verworfen. 

Damit die Notebooks sich gegenseitig anpingen können, müssen auf beiden Routern entsprechende Routen konfiguriert werden. 

## Routen konfigurieren. 

Damit das Notebook `N1` und `N2` sich gegenseitig anpingen können, müssen entsprechende Routen konfiguriert werden. 

**Ausgangslage:**
 - Der Router `R1` weiss nicht wo das Subnetz Z ist. 
 - Der Router `R2` weiss nicht wo das Subnetz X ist. 

Zuerst konfigurieren wir im Router `R1` eine Route nach Subnetz Z. Diese lautet in Worte: "Alle Pakete nach Subnetz Z schickst du zum Router `R2`."

Damit die Antwort auf den Ping auf seinen Weg zurückfindet, muss auf `R2` auch einen Route ins Subnet X konfiguriert werden. 

![Route hinzufügen](media/addroute.png)
<br>*Abbildung 5: Beispiel wie in WinBox eine Route konfiguriert werden kann.*

**💪 Challenge 5:**  Konfiguriere auf beiden Routern jeweils eine statische Route, sodass sich die Notebooks anschliessend gegenseitig anpingen können. Verwende dazu die Informationen aus *Abbildung 5*. Beachte jedoch das die effektiv zu verwendenden IPv4-Adressen **nicht** den Adressen aus der Abbildung entsprechen!


## Abschluss

Die Übung ist erfolgreich abgeschlossen, wenn sich die Notebooks über zwei Router hinweg gegenseitig anpingen können. 