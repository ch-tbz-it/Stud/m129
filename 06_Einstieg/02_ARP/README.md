# ARP

| Lerneinheit  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  1 Lektion |
| Ziele | - Sinn & Zweck des ARP Protokolls verstehen.<br> - Wichtige Eigenschaften des APR-Protokolls kennen. |

## Theorie

Das [ARP Protokoll](https://de.wikipedia.org/wiki/Address_Resolution_Protocol) wird benötigt, um auf einen Ethernet II Netzwerk mit IPv4 zu kommunizieren. 

**Ausgangslage:** Wenn ein Hosts, die z.B. über einen Switch miteinander verbunden sind, mit einem anderen Host über IPv4 kommunizieren möchten, benötigt dieser die MAC-Adresse des anderen Teilnehmers. Mit dem ARP-Protokoll kann der Host die MAC-Adresse des anderen Hosts herausfinden. 

### Ressourcen: 
 - [Video -  ARP Explained | Address Resolution Protocol ](https://www.youtube.com/watch?v=tXzKjtMHgWI)
 - [Video - Address Resolution Protocol: ARP packet structure and demonstration ](https://www.youtube.com/watch?v=Hudi5Ncs8EQ)

## Übung

In dieser Übung analysierst du einen ARP-*Sample Capture* mit Wireshark und beantwortest dazu Fragen. Halte die Fragen und Antworten in deinem Lernjournal fest. 

### Voraussetzungen
 - [Wireshark](https://www.wireshark.org/download.html) auf dem eigenen PC installiert
 - [ARP example capture](./ARP_example_capture.pcapng) im Wireshark geöffnet


### Vorgehen
 - Vergewissere dich über dein Lernziel. Am Ende der Übung solltest du in der Lage sein, folgende Fragen zu beantworten: 
   - Was ist ARP?
   - Wozu braucht es ARP?
   - Weshalb ist ARP in der Netzwerktechnik relevant?
   - Wie funktioniert ARP. 
 - Informiere mich zum Thema ARP im Internet (Siehe Link zu Video oben)
 - Bereite deine Lernumgebung gemäss den oben beschriebenen Voraussetzungen vor.
 - Gehe eine Frage (unten) nach der anderen durch und versuche sie zu beantworten. 

### Fragen

Das Ziel der nachfolgenden Fragen ist es, ein besseres Verständnis von ARP zu erlangen. 

 - Welche beide IPv4-Adressen bzw. Hosts möchten miteinander kommunizieren?
 - Welche MAC-Adresse und IP-Adresse haben die beiden Hosts?
 - Welcher Befehl wurde höchstwahrscheinlich auf dem einten Host ausgeführt?
 - An welche MAC-Adresse wurde der ARP-Request geschickt?
 - An welche MAC-Adresse geht der ARP reply und weshalb macht dieser Empfänger Sinn?
 - Was ist die RTT des ersten erfolgreichen "ICMP - ECHO (ping)"?
 - Welche beiden Layer gemäss [OSI-Referenzmodell](https://de.wikipedia.org/wiki/OSI-Modell) verknüpft ARP?
 - Weshalb können zwei über einen Ethernet-Switch verbundene Hosts ohne ARP nicht mit IPv4 kommunizieren?

Halten Sie die Fragen, dazugehörige Antworten und weitere Erkenntnisse in ihrem Lernjournal fest une besprechen Sie Ihre Lösung anschliessend mit der Lehrperson. 


## Wireshark
![Wireshark](./media/wireshark-overview.JPG)