# Cisco Packet Tracer Einstieg

Der/Die Netzwerktechniker*in beschäftigt sich nicht nur mit der Neukonfiguration von Netzwerken, sondern muss häufig auch Störungen in Netzwerken beheben. Dazu ist es hilfreich ein paar grundsätzliche Herangehensweisen zu kennen. In dieser Übung lernen Sie, wie einfache Konfigurationsfehler in einem Netzwerk identifiziert und behoben werden können. Die gesamte Übung wird mit dem Cisco Packet Tracer und diesem Dokument gelöst. 

## Lernziele
 - Kennt die gängigen Vorgehensweisen und Methoden, einen Test mit den dazugehörigen Testszenarien durchzuführen.
 - Kennt die Vorgehensweise, einen geplanten funktionalen Test durchzuführen.
 - Kennt die Symptome der wichtigsten Fehler in einem Netzwerk und kann mögliche Ursachen (Konfigurationsfehler, Fehler bei der Verkabelung etc.) dafür beschreiben.
 - Kennt die Möglichkeiten das Netzwerk und all seine Elemente zu testen und die Ergebnis-se zu dokumentieren.

## Übungsziele
 - Alle Fehler im Netzwerk identifiziert und protokolliert.
 - Netzwerk analysiert
 - Alle Fehler im Netzwerk behoben
 - Netzwerk gemäss Anweisungen erweitert

## Voraussetzung
 - Cisco Packet Tracer auf dem eigenen Laptop installiert. 

## Vorgehen
 - [Aufgabenbeschreibung und Laborjournal Datei](./01_Fehlersuche%20im%20Netzwerk%20mit%20Cisco%20Packet%20Tracer%20V3.docx) herunterladen und durchlesen
 - [PKA Labor-Datei](./Labor.pkt) herunterladen und mit Cisco Packet Tracer öffnen
 - Aufgabe gemäss Aufgabenbeschreibungen durcharbeiten
 - Abgeschlossene Aufgabe mit Lehrperson besprechen

