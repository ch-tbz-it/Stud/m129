![Screenshot BeARouter](media/BeARouter.PNG)

# Übung - BeARouter
| Lerneinheit  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Praktische Übung |
| Zeitbudget  |  2 Lektion |
| Ziele | Sie verstehen das grundlegende Funktionsprinzip von Routing. |

## Vorgehen
 - Laden Sie sich auf https://github.com/muqiuq/BeASwitch/releases die neuste Version von BeARouter herunter. 
 - Starten Sie das EXE (Der Anwendung muss manuell vertraut werden!)<br>**Eventuell müssen Sie noch .NET Core installieren.**
 - Versuchen Sie im *Practice*-Modus mit *Trial-and-Error* nachzuvollziehen wie die Anwendung funktioniert bzw. wie ein Routing-Chip Routing-Entscheidungen fällt. 
 - Sobald Sie das Prinzip verstanden haben, wechseln Sie in den Exam Mode. Informieren Sie sich bei der Lehrperson, welche Werte einzugeben sind. 
 - Am Schluss des Exam Modus erhalten Sie ein *SuccessCertificate*, dass sie einerseits auf ihrem Computer abspeichern und andererseits automatisch übermitteln. 
 
*Hinweis: Je nach Lehrperson ist BeARouter ein Teil der Leistungsbeurteilung. Informieren Sie sich bei Unsicherheiten bei der Lehrperson.*