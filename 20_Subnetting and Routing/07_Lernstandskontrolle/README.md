# IPv4 Rechnen - Lernstandskontrolle

| Lerneinheit  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Prüfe, ob du alle IPv4 "Grundrechenoperationen" beherrscht |
| Zeitbudget  |  30 Minuten |
| Ziele | Den eigenen Lernstand hinsichtlich IPv4 "Grundrechenoperationen" prüfen. |

In der Regel prüfen Lehrpersonen in einer Prüfung, ob die Lernenden die IPv4 Grundrechenoperationen beherrschen. 

In dieser Lerneinheit stehen 200 Probeprüfungen, bzw. Lernstandskontrollen in PDF Form bereit. 

Im PDF [M129 IPv4 Rechnen Lernstandskontrolle 0bis200](./M129%20IPv4%20Rechnen%20Lernstandskontrolle%200bis200.pdf) sind 200 Lernstandskontrollen zu finden. 

Im PDF [M129 IPv4 Rechnen Lernstandskontrolle Lösungen 0bis200](./M129%20IPv4%20Rechnen%20Lernstandskontrolle%20Lösungen%200bis200.pdf) sind die Lösungen zu finden. 

Jede Lernstandskontrolle bzw. jede einzelne Seite hat eine Laufnummer in der oberen rechten Ecke. Die entsprechende Lösung hat dieselbe Nummer:

![Laufnummer](./media/Laufnummer.png)

**Tipp**: Prüfe welche Hilfsmittel an der notenrelevanten Prüfung zugelassen sind und verwende beim Üben dieselben Hilfsmittel. 