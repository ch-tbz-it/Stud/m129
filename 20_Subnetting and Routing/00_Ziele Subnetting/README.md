# Ziele von Subnetting & Routing

## Einleitung

Für eine effiziente Zustellung von Briefen ist es entscheidend, dass die Post über vollständige und korrekte Informationen verfügt, wohin der Brief gesendet werden soll. Dies wird durch die Angabe einer präzisen Anschrift ermöglicht, die folgende Elemente umfasst: das Land, die Stadt, die Strasse und den Namen des Empfängers wird auf dem Couvert angebracht. Ergänzt wird diese Adresse durch die Postleitzahl, die für die Post von grosser Bedeutung ist. Die Schweizerische Post hat das Land in verschiedene Regionen eingeteilt, wobei jeder Region eine spezifische, vierstellige Postleitzahl zugewiesen ist. Diese strukturierte Vorgehensweise erleichtert die Sortier- und Zustellprozesse erheblich.

![PLZ](./media/PLZ.JPG)
<br>*Abbildung 1: Der hierarchische Aufbau des Post-Routings mit Router-Symbolen als fiktive Verteilzentren*

Das Adressierungssystem der Post ist hierarchisch organisiert, was bedeutende Vorteile mit sich bringt. Ein solches System ermöglicht es, dass Zustellpersonal nicht die genaue Endadresse eines internationalen Briefes kennen muss. Zum Beispiel muss ein Zusteller in Zürich lediglich wissen, dass ein für Deutschland bestimmter Brief zunächst an ein zentrales Verteilzentrum in der Schweiz gesendet wird. Von dort aus wird der Brief an ein entsprechendes Verteilzentrum in Deutschland weitergeleitet, welches wiederum den Brief an regionale Verteilzentren innerhalb Deutschlands verteilt. Somit ist jeder Schritt im Zustellprozess eines Briefes nur mit dem unmittelbar notwendigen Wissen ausgestattet, was den Gesamtprozess effizienter und fehlerresistenter macht.

Durch dieses systematische und hierarchische Adress- und Verteilsystem kann die Post die Briefe effizient und zielgerichtet an die Empfänger zustellen. Es ermöglicht eine strukturierte und schnelle Bearbeitung der Sendungen, vom Eingang bei der Post bis zur finalen Zustellung beim Empfänger.

Die Übermittlung eines Briefes von einem Ausgangspunkt zu einem Ziel nennen wir Routing. 

## Vorteile von Subnetting - Zusammenfassen von Routen

![Wegweiser 1](./media/wegweiser1.JPG)
<br>*Abbildung 2: Wegweiser mit und ohne Zusammenfassung*

IPv4-Subnetting kann insbesondere zu beginn aufgrund der Notation verwirrend sein. Deshalb werden die Vorteile von IPv4-Subnetting mit einer Analogie veranschaulicht. 

Bei der Betrachtung der beiden Wegweiser Varianten in *Abbildung 2* kann intuitiv festgestellt werden, dass der Wegweiser rechts **mit** Zusammenfassung wesentlich schneller überprüft werden kann als Wegweiser links **ohne** Zusammenfassung. Die Frage ist nun, weshalb es auch für Computer einfacher ist, den Wegweiser **mit** Zusammenfassung zu prüfen. 

Beispiel: Nehmen wir an, wir wollen nach **N** gehen. Dafür müssen wir alle Wegweiser-Schilder überprüfen, damit wir wissen, ob wir nach links oder rechts gehen müssen. 

```
Ist O gleich mein Ziel N? Nein
Ist H gleich mein Ziel N? Nein
Ist G gleich mein Ziel N? Nein
Ist P gleich mein Ziel N? Nein
Ist R gleich mein Ziel N? Nein
Ist E gleich mein Ziel N? Nein
Ist T gleich mein Ziel N? Nein
Ist S gleich mein Ziel N? Nein
Ist C gleich mein Ziel N? Nein
Ist M gleich mein Ziel N? Nein
Ist I gleich mein Ziel N? Nein
Ist B gleich mein Ziel N? Nein
Ist A gleich mein Ziel N? Nein
Ist K gleich mein Ziel N? Nein
Ist D gleich mein Ziel N? Nein
Ist N gleich mein Ziel N? Ja!
```

Insgesamt mussten wir 16 Schilder prüfen und mit unserem Ziel vergleichen, bevor wir mit Gewissheit feststellen konnten, dass wir nach rechts weitergehen müssen. 

Machen wir nun dasselbe mit dem Wegweiser **mit** Zusammenfassung:

```
Ist mein Ziel N in Bereich von E bis H? Nein
Ist mein Ziel N in Bereich von O bis T? Nein
Ist mein Ziel N in Bereich von A bis D? Nein
Ist mein Ziel N in Bereich von I bis N? Ja!
```

Danke der Zusammenfassung mussten wir nur noch 4 Schilder prüfen. 

Für Computer ist es wesentlich effizienter einfache mathematische Verfahren einzusetzen, als lange Listen durchzugehen. Zudem benötigen zusammengefasste Listen wesentlich weniger Speicher. 

![Wegweiser IP](./media/wegweiser2.JPG)
<br>*Abbildung 3: Wegweiser für IPv4 mit und ohne Zusammenfassung*

Im Falle von IPv4-Adressen ist die Zusammenfassung noch viel extremer. Insgesamt stehen bei IPv4 2^32 = 4'294'967'296 Adressen zur Verfügung. Müsste jede Adresse einzeln als Wegweiser angebracht werden, müsste man bei jedem Wegpunkt 4'294'967'296 Schilder anbringen. Stattdessen können wir diese Wegweiser in *Subnets* zusammenfassen. 

**Ein Subnet, Subnetz bzw. Teilnetz ist ein physikalisches Segment eines Netzwerks, in dem IP-Adressen mit der gleichen Netzwerkadresse benutzt werden. Diese Teilnetze können über Router miteinander verbunden werden und bilden dann ein grosses zusammenhängendes Netzwerk.**

## Notation

Im Subnetting wird in der Computerwelt aus technischen Gründen nicht die Notation `von X bis Y` verwendet. Stattdessen definieren wir Teilnetze mit `Netz/Maske`. 

In den nachfolgenden Abschnitten und Aufgaben erlernst du die Grundlagen für Subnetting. 
