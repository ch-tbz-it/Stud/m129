# Lernvideos IPv4 Berechnungen

Folgende Konzepte hinsichtlich IPv4-Berechnungen dienen als Grundlagen für das Verständnis von IPv4:
 - Die Umrechnung einer IPv4-Adresse vo der dezimalen in die Binäre Form:
   - Beispiel: Aus `192.168.100.5` wird binär `11000000 10101000 01100100 00000101`
 - Die Umrechnung einer IPv4-Subnetzmaske vom Dezimalformat in das "CIDR"-Format.
   - Beispiel: Aus `255.255.192.0 wird /18` und umgekehrt. 
 - Aus einer beliebigen IPv4 Adresse und einer Subnetzmaske, die **Netz-Adresse ermitteln**
   - Beispiel: Aus `192.168.100.5/18` wird `192.168.64.0`
 - Aus einer beliebigen IPv4 Adresse und einer Subnetzmaske, die **Broadcast-Adresse ermitteln**
   - Beispiel: Aus `192.168.100.5/18` wird `192.168.127.255`
 - Feststellen, ob eine IPv4-Adresse Teil eines IPv4 Subnetzes ist.
   - Beispiel: `192.168.100.5` ist Teil von `192.168.64.0/18`, aber `192.168.3.1` ist nicht Teil davon. 

Nach Abschluss dieser Übung soll für jedes der aufgeführten Konzepte ein Lernvideo vorhanden sein. Jede Gruppe erstellt ein Video zu einem anderen Thema. 

# Rahmenbedingungen
 - Pro Gruppe 2 Personen
 - Zeitbudget: 3 Lektionen
 - Videolänge: 3 bis 5 Minuten

# Idee
![Idee](./media/Beispiel.JPG)
 - Im Bild sichtbar ist ein Blatt-Papier auf dem eine IPv4-Berechnung durchgeführt wird (Siehe Grafik oben). 
 - Im Video erklärt eine Person der anderen, wie Sie vorgehen muss. Dazu stellt eine Person Fragen (z.B. "Ich habe eine IPv4-Adresse und möchte nun die binäre Form berechnen. Wie gehe ich vor?"), die andere ist der/die Erklärer*in und gibt Antwort (z.B. "Jede der vier Dezimalzahlen müssen wir in die Binäre Form umwandeln. Jede Dezimalzahl steht für ein Byte. Ein Byte kann Werte von 0 bis 255 annehmen. Der Wert 255 benötigt 8 bits, d.h. 0 und 1, um in binärer Schreibweise darzustellen. Als erstes schreibt du die IPv4-Adresse so aus, dass du zwischen den Dezimalzahlen genug Platz hast um die Binäre-Form aufzuschreiben." ). 
 - Im Video wird nebst der Berechnung "von Hand" auch die Berechnung mithilfe von nützlichen Tools gezeigt. Damit soll Verständnis darüber geschaffen werden, was die Tools überhaupt machen. 

# Ziel des Videos
**Videoanleitung**: Der Betrachter des Videos sollte anschliessend in der Lage sein, mithilfe des Videos eine beliebige Aufgabe dieses Types von Hand lösen zu können.

# Vorgehen
 - Bilden Sie eine Gruppe gemäss Rahmenbedingungen
 - Wählen Sie eines der oben aufgeführten Konzepte aus. Alle Themen sollten in der Klasse abgedeckt sein.
 - Erstellen Sie ein Mindmap und überlegen Sie sich was im Video alles erklärt werden muss. <br>**Wichtig:** Nehmen Sie im Video ein anderes Beispiel!
 - Besprechen Sie das Mindmap mit der Lehrperson
 - Definieren Sie "Einleitung", "Inhalt" und "Schluss" für ihr Video und halten Sie das in einem Dokument fest.
 - Schreiben Sie den gesamten Dialog auf. Schreiben Sie jeweils dazu, was im im Video zu diesem Zeitpunkt jeweils gezeigt werden soll. 
 - Besprechen Sie Ihren Plan mit der Lehrperson
 - Drehen und Schneiden Sie das Video
 
# Ressourcen
