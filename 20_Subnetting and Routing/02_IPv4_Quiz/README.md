# DoAIPv4Quiz - IPv4 Übungsaufgaben

![DoAIPv4Quiz](./media/beaquiz.JPG)

IPv4 verstehen, bedeutet (unter anderem) die wichtigsten IPv4-Berechnungen zu verstehen. 

Mit **BeARouter - IPv4 Quiz** bietet Übungsaufgaben zu den wichtigsten IPv4-Berechnungen. Da die Aufgaben mit einem Zufallsgenerator erstellt werden, steht eine fast unendliche Anzahl an Übungsaufgaben zur Verfügung. 

Das Grundkonzept dieser Übungsaufgabe: Üben bis verstanden!

| Lerneinheit  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | IPv4 Berechnungen üben bis Verstanden |
| Zeitbudget  |  2 Lektion |
| Ziele | Alle IPv4-Berechnungen beherrschen |

## Vorgehen
 - BeARouter von [GitHub - BeASwitch - Releases](https://github.com/muqiuq/BeASwitch/releases) herunterladen. 
 - BeARouter starten und **IPQuiz** auswählen.
 - *Practice Mode* auswählen und *Start* drücken

![Disable IPv6](./media/options.JPG)
 - Unter *Options*, *IPv6* deaktivieren. 
 - Solange üben, bis alle Aufgabentypen verstanden wurden.
 - Programm im *Exam mode* neustarten (37/40). 
 - Durchspielen bis das Ziel erreicht wurde und anschliessend Zertifikat übermitteln. 

