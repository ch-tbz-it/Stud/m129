# IPv4 <!-- omit in toc -->

# Lernziele <!-- omit in toc --> 
 - Sie kennen und verstehen den Aufbau einer IPv4 Adresse.
 - Sie können zwischen privaten, reservierten und öffentlichen IPv4-Adressen unterscheiden.
 - Sie können aus einer IPv4 und [CIDR](https://de.wikipedia.org/wiki/Classless_Inter-Domain_Routing) bzw. (Maske) die Netz- und Broadcast-Adresse berechnen.
 - Eine IPv4-Adresse einer Route in einer Routing-Tabelle zuordnen.
 - Sie können zwischen einer IPv4-Adresse und einer IPv6-Adresse unterscheiden. 
 - Sie können eine Netzmaske in der CIDR-Notation (/24), Binären und IP schreiben und umrechnen.

# Inhaltsverzeichnis <!-- omit in toc -->
- [1. IPv4 - Umrechung Dezial \<-\> Binär](#1-ipv4---umrechung-dezial---binär)
- [2. IPv4 - Netz-, Broadcast-, und Host-Adressen bestimmen](#2-ipv4---netz--broadcast--und-host-adressen-bestimmen)
  - [2.1. Beispiel 1 - 192.168.6.4/24](#21-beispiel-1---1921686424)
  - [2.2. Beispiel 2 - 192.168.128.255/19](#22-beispiel-2---19216812825519)
- [3. IPv4 Netzmasken umrechnen](#3-ipv4-netzmasken-umrechnen)
  - [3.1. Ausgangslage - Netzmaske liegt als IPv4-Adresse vor](#31-ausgangslage---netzmaske-liegt-als-ipv4-adresse-vor)
    - [3.1.1. Vorgehen](#311-vorgehen)
  - [3.2. Ungültige Netzmaske](#32-ungültige-netzmaske)
    - [3.2.1. Vorgehen](#321-vorgehen)
- [4. IPv4 Subnetze aufteilen](#4-ipv4-subnetze-aufteilen)
  - [4.1. Vorgehen](#41-vorgehen)
  - [4.2. Tool](#42-tool)
- [5. Subnetze aggregieren](#5-subnetze-aggregieren)
- [6. Ressourcen](#6-ressourcen)
- [7. Links](#7-links)

# 1. IPv4 - Umrechung Dezial <-> Binär
Im nachfolgenden Beispiel ist die Umrechnung einer IPv4 Adresse dargestellt. 
```
    192   .    168    .     1     .     5
1100 0000 . 1010 1000 . 0000 0001 . 0000 0101
```
Um die einzelnen Dezimalzahlen, die jeweils **1 Byte** repräsentieren, umzurechnen, empfiehlt sich ipcalc zu verwenden oder der Windows Rechner. 

---

![media](media/windowscalculator.PNG)
Der Standard-Windows-Rechner (gilt auch für OSX) lässt sich über das Menü (oben links) in den *Programmer*-Modus umschalten. 

---

![media](media/ipcalc.PNG)
Das CLI Tool *ipcalc* rechnet nicht nur Subnetze aus, sondern zeigt auch gleich die Binäre Schreibweise. 

# 2. IPv4 - Netz-, Broadcast-, und Host-Adressen bestimmen
Ein Subnetz wird durch die Netzadresse und einer Netzmaske bestimmt. Ist die IPv4-Adresse keine Netzadresse, sondern eine Hostsadresse, lässt sich mithilfe der Subnetzmaske, die Netzadresse und die Broadcast-Adresse bestimmen. 

## 2.1. Beispiel 1 - 192.168.6.4/24
Nehmen wir die IPv4-Adresse *192.168.6.4/24* und beantworten dazu, die relevanten Fragen:
 - Ist die vorliegende IPv4-Adresse eine Netz-, Broadcast- oder Host-Adresse?
 - Wie lautet die Netzadresse?
 - Wie lautet die Broadcast-Adresse?
 - Wie viele Hosts können adressiert werden?
 - Welche IPv4-Adressen stehen für die Host-Adressierung zur Verfügung? 

```
# Schritt 1: IPv4 Adresse in binäre Form umwandeln
Address:   192.168.6.4          11000000.10101000.00000110. 00000100

# Schritt 2: Netzmaske und Wildcard in binäre Form umwandeln
Netmask:   255.255.255.0 = 24   11111111.11111111.11111111. 00000000
                                <=        24 Eins       =>
Wildcard:  0.0.0.255            00000000.00000000.00000000. 11111111
                                                            <8 Eins>
                                24 Bits + 8 Bits = 32 Bits = 4 Bytes

# Schritt 3: Logisch UND (&) mit der IPv4-Adresse & Netzmaske ergibt Netzadresse
Address:   192.168.6.4          11000000.10101000.00000110. 00000100
 & Netmask:   255.255.255.0     11111111.11111111.11111111. 00000000
 = Network:   192.168.6.0/24    11000000.10101000.00000110. 00000000

# Schritt 4: Netzmaske + 1 ergibt HostMin
Network:   192.168.6.0/24       11000000.10101000.00000110. 00000000
+ 1                             00000000.00000000.00000000. 00000001
= HostMin:   192.168.6.1          11000000.10101000.00000110. 00000001

# Schritt 5: Netzmaske & Wildcard = Broadcast
Network:   192.168.6.0/24       11000000.10101000.00000110. 00000000
& Wildcard                      00000000.00000000.00000000. 11111111
 = Broadcast: 192.168.6.255     11000000.10101000.00000110. 11111111

# Schritt 6: Broadcast - 1 ergibt HostMax
Broadcast: 192.168.6.255     11000000.10101000.00000110. 11111111
 -1                          00000000.00000000.00000000. 00000001
 = HostMax: 192.168.6.254    11000000.10101000.00000110. 11111110

# Schritt 7: Anzahl möglicher Hosts bestimmen: 2^(32-CIDR) -2 = Anzahl Hosts
2^(32-24) - 2 = 254
```

## 2.2. Beispiel 2 - 192.168.128.255/19
Ein wenig schwieriger wird die Umrechnung, wenn die Netzmaske nicht mehr /24 oder /16 ist, bzw. nicht mehr durch 8 teilbar ist. Dann ist die Umrechnung in die binäre Form meist das einfachste Mittel, um die Netz-, Broadcast-, und Host-Adressen, sowie die Anzahl Hosts zu bestimmen. 

```
Address:   192.168.128.255      11000000.10101000.100 00000.11111111
Netmask:   255.255.224.0 = 19   11111111.11111111.111 00000.00000000
Wildcard:  0.0.31.255           00000000.00000000.000 11111.11111111
=>
Network:   192.168.128.0/19     11000000.10101000.100 00000.00000000
HostMin:   192.168.128.1        11000000.10101000.100 00000.00000001
HostMax:   192.168.159.254      11000000.10101000.100 11111.11111110
Broadcast: 192.168.159.255      11000000.10101000.100 11111.11111111
Hosts/Net: 8190
```

# 3. IPv4 Netzmasken umrechnen

## 3.1. Ausgangslage - Netzmaske liegt als IPv4-Adresse vor
In diesem Beispiel bestimmen wir den CIDR und die binäre Form, ausgehend von der Netzmaske als IPv4-Adresse.

### 3.1.1. Vorgehen
 1. IPv4-Adresse aufschreiben mit genügend Abstand.
 2. Für jedes einzelne der 4 Bytes den binären Wert bestimmen. 
 3. Die Anzahl 1 von links Zählen. 

```
Dezimal :     255     .    255    .    224    .     0
Binär   :   1111 1111 . 1111 1111 . 1110 0000 . 0000 0000
CIDR    :            19 Eins          >
```

## 3.2. Ungültige Netzmaske
Wir wollen herausfinden, ob die IPv4-Adresse *255.255.255.160* eine gültige Netzmaske ist. 

### 3.2.1. Vorgehen
1. IPv4-Adresse aufschreiben mit genügend Abstand.
2. Für jedes einzelne der 4 Bytes den binären Wert bestimmen. 
3. Wenn zwischen dem von links aus gesehen letzten Eins und dem linken Anfang eine 0 ist, dann ist es keine gültige Netzmaske.

```
Dezimal :     255     .    255    .    255    .     160
Binär   :   1111 1111 . 1111 1111 . 1111 1111 . 1010 0000
                                                 ^ Diese Null steht zwischen dem letzten Eins und dem Anfang. ==> Keine gültige Netzmaske. 
```

Aufgrund dieser Regel, lassen sich alle möglichen binären und dezimalen Werte bestimmen, die in einem Byte einer Netzmaske in IPv4-Adresse-Darstellung vorkommen können.

![media](media/possiblenetmaskvalues.PNG)
Ausschnitt aus der Excel-Datei *IPv4 Calculator.xlsx*.

# 4. IPv4 Subnetze aufteilen
![media](media/kuchensubnetting.JPG)

Die Aufteilung eines Netzwerks in kleinere Segmente lässt sich mit dem Zerteilen eines Kuchens in mehrere kleinere Stücke vergleichen. In dieser Abbildung wird das Netzwerk 192.168.128.0/22 mit 1024 Adressen (davon 1022 für Hosts nutzbar) in vier /24 Netzwerke mit jeweils 256 Adressen (davon 254 für Hosts nutzbar) aufgeteilt.


In der nachfolgenden Grafik wird das Subnetz 192.168.192.0/18 durch 6 Subnetze geteilt. 

![media](media/subnetz_teilen.PNG)

## 4.1. Vorgehen 
 1. Neue Netzmaske berechnen: Da 6 keine 2er Potenz ist, nehme die nächst grössere Zahl, die eine 2er Potenz ist. Bestimme den Exponenten.<br> 8 = 2^3 => Exponent: 3
 2. Die Subnetzbits lassen sich aus dem Exponent bestimmen (Subnetzbits ^= Exponent)
 2. Rechne die neue Subnetzmaske aus:<br>[Aktuelle Subnetzmaske als CIDR] + [Exponent von Schritt 1] = [Neuer CIDR]
 3. Zähle Binär innerhalb der Subnetzbits bis zum Maximum ( von 000 zu 111).
 4. Rechne für jedes neue Subnetz die entsprechende IPv4-Adresse aus.

## 4.2. Tool
Der *Visual Subnet Calculator* (https://www.davidc.net/sites/default/subnets/subnets.html) ist ein praktisches Tool, um ein Subnetz aufzuteilen.
![media](media/visualsubnetcalculator.PNG)

# 5. Subnetze aggregieren
![Beispiel Netzwerk](media/beispielnetz.PNG)
In der oben dargestellten Topologie sind zwei Standorte dargestellt, die je drei /24 Subnetze verwenden.

Wenn wir alle Netze von überall erreichen möchten, könnten folgende Routen angelegt werden:
```
# Router A (Suboptimale Konfiguration)
192.168.12.0/24 via 192.168.1.18
192.168.13.0/24 via 192.168.1.18
192.168.14.0/24 via 192.168.1.18

# Router B (Suboptimale Konfiguration)
192.168.8.0/24 via 192.168.1.17
192.168.9.0/24 via 192.168.1.17
192.168.10.0/24 via 192.168.1.17
```

Beim Festlegen der Subnetze wurde darauf geachtet, dass sich die Subnetze aggregieren lassen.
```
192.168.8.0/24   =>   11000000.10101000.00001000.00000000
192.168.9.0/24   =>   11000000.10101000.00001001.00000000
192.168.10.0/24  =>   11000000.10101000.00001010.00000000
  Bis zu diesem Bit sind die Adressen gleich ┘
 = 22 gleiche Bits
  22 Bits              >
11000000.10101000.00001000.00000000 => 192.168.8.0/22

192.168.8.0/22 umfasst alle Hosts von
HostMin 192.168.8.1
HostMax 192.168.11.254
```

# 6. Ressourcen
 - Videos zum Thema Subnetting:
   - https://web.microsoftstream.com/video/95c020f2-44a2-448b-b7ba-1f33ac1180d9


# 7. Links
 - https://de.wikipedia.org/wiki/Classless_Inter-Domain_Routing
 - https://www.davidc.net/sites/default/subnets/subnets.html
 - https://help.ui.com/hc/en-us/articles/115005968648-Intro-to-Networking-IPv4-Addressing-Subnets