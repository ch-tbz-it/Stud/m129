# Subnetting

| Lerneinheit  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Subnetzplanung Üben |
| Zeitbudget  |  2 Lektion |
| Ziele | Kann für ein kleines privates Netzwerk eine Subnetzplanung vornehmen. |

Subnetting kann auf unterschiedliche Arten erfolgen. Die Art und Weise hängt vom spezifischen Anwendungsfall und eingesetzten Technologien ab. Bei der Planung von Netzwerken ist es zentral, dass die potentiell zukünftige Entwicklung miteingeplant wird. Eine gute Netzwerkplanung kann z.B. beim Wachstum einer Firma skalieren. In der Praxis findet man häufig "historisch gewachsene" Netzwerke mit einer entsprechend inkonsistenten Netzwerkplanung.

Dieser Übungsblock enthält Subnetting-Übungsaufgaben.


## Beispiel
![Example](./media/example.JPG)


**Merke:**
 - Alle Client-Access Netzwerke sind `/24`. Das ist eine Design-Entscheidung und nicht zwingend. 
 - Alle Point-To-Point Netzwerke sind `/30` von 4 Adressen sind zwei nutzbar: Reicht für ein Point-to-Point Netzwerke. 
 - Die Netze `192.168.8.0/24`, `192.168.9.0/24`, `192.168.10.0/24`, `192.168.11.0/30`, `192.168.11.4/30` können in `192.168.8.0/22` zusammengefasst werden. Das hat den Vorteil, dass es vom Router `R3` nur eine Route für all diese Netze benötigt. 
 - Die Netze `192.168.12.0/24`, `192.168.13.0/24`, `192.168.14.0/24`, `192.168.15.0/30` können in `192.168.12.0/22` zusammengefasst werden. Das hat den Vorteil, dass es vom Router `R3` nur eine Route für all diese Netze benötigt. 
 - Wenn auf Router `R1` oder `R4` ein `/24` Netz hinzugefügt werden muss, ist das nicht möglich. Die Nachhaltigkeit ist begrenzt.

In Aufgabe 4 gibt es zu diesem Netzwerk weiterführende Fragen. 


## Aufgabe 1
![Aufgabe 1](./media/Aufgabe1.JPG)

 - Definiere ein übergeordnetes `/20` in einem [Privaten IPv4 Range](https://de.wikipedia.org/wiki/Private_IP-Adresse).
 - Lege die 5 Client Access Netzwerke und die zwei Point-To-Point Netze fest.  

## Aufgabe 2
![Aufgabe 2](./media/Aufgabe2.JPG)

 - Definiere ein übergeordnetes `/21` in einem [Privaten IPv4 Range](https://de.wikipedia.org/wiki/Private_IP-Adresse).
 - Lege die 2 Client Access Netzwerke und die zwei Point-To-Point Netze fest.  

# Aufgabe 3
![Aufgabe 3](./media/Aufgabe3.JPG)

- Definiere ein übergeordnetes `/16` Netzwerk und nehme deine Planung so vor, dass Sie sich jederzeit erweitern lässt. 
- Lege alle benötigten *Point-To-Point* und *Access Netzwerke* fest.

# Aufgabe 4 - Fortgeschritten - Campus LAN
![Aufgabe 4](./media/Aufgabe4.png)

In dieser Aufgabe sind Subnetze für ein Netzwerk festzulegen, welches nach den Regeln des klassischen Campus LAN Designs aufgebaut wurde. (siehe [Cisco Campus LAN and Wireless LAN Solution Design Guide](https://www.cisco.com/c/en/us/td/docs/solutions/CVD/Campus/cisco-campus-lan-wlan-design-guide.html)). 

 - Definiere ein übergeordnetes `/16` in einem [Privaten IPv4 Range](https://de.wikipedia.org/wiki/Private_IP-Adresse). Alle im Netzwerk verwendeten Subnetze müssen aus diesem übergeordneten Subnetz stammen. 
 - Verwende für die Netzwerke zwischen den Routern (C-R1, C-R2, D-R1, D-R2) `/30` Netze, welche alle aus demselben `/24` Subnetz stammen. 
 - Der Router für die vier VLANs (VLAN10, VLAN20, VLAN30, VLAN40) sind die zwei *Layer 3 Switche* (Eine Kombination aus Router und Switch, oder einfach ein Router mit "vielen" Ports). Diese nehmen hier die Rolle der *Distribution Switche* (D-R1, D-R2) ein. 
 - Definiere jeweils ein `/24`-Subnetz für die vier VLANs. Diese sollten in einem `/21` zusammengefasst werden können. 
 - Die VLAN-ID (z.B. bei VLAN10 ist es die ID 10) sollte für eine einfache Verwaltung (Best Practice) in der IPv4-Adresse abgebildet werden. Beispiel: VLAN10 = 192.168.10.0/24, VLAN20 = 192.168.20.0/24, usw.) So lässt sich die VLAN-ID aus dem dritten Octet ablesen, bzw. kann die VLAN-ID aus dem Subnetz abgeleitet werden.

# Aufgabe 5 - Fortgeschritten - Subnetz Optimierung
Das Netzwerk aus dem *Beispiel* (ganz zu Beginn dieses Dokumentes) hat einen beträchtlichen Nachteil: Kommt auf Router `R1` oder auf Router `R4` ein weiteres `/24` hinzu, muss entweder die gesamte Adressierung angepasst werden oder zusätzliche Subnetze aus einem noch freien Bereich verwendet werden. Letzteres führt zu einem "Gebastel". Dies wird dann gerne auch als "historisch gewachsen" bezeichnet. 

Passe das Subnetting aus dem Beispiel so an, dass das Erweiterungen durch zusätzliche `/24` Subnetze nicht gleich zu einer kompletten Neuadressierung oder einem "Gebastel" führen. 

